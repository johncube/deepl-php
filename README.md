# README #

### What is this repository for? ###

It is using deepL api to translate your text

### How do I get set up? ###

composer require johncube/deepl

### functions ###

deepl(key, text, target_language, source_language = null)
source_language (optional) - Language of the text to be translated. Options currently available:
    "BG" - Bulgarian
    "CS" - Czech
    "DA" - Danish
    "DE" - German
    "EL" - Greek
    "EN" - English
    "ES" - Spanish
    "ET" - Estonian
    "FI" - Finnish
    "FR" - French
    "HU" - Hungarian
    "IT" - Italian
    "JA" - Japanese
    "LT" - Lithuanian
    "LV" - Latvian
    "NL" - Dutch
    "PL" - Polish
    "PT" - Portuguese (all Portuguese varieties mixed)
    "RO" - Romanian
    "RU" - Russian
    "SK" - Slovak
    "SL" - Slovenian
    "SV" - Swedish
    "ZH" - Chinese
    If this parameter is omitted, the API will attempt to detect the language of the text and translate it.

target_lang - The language into which the text should be translated. Options currently available:
    "BG" - Bulgarian
    "CS" - Czech
    "DA" - Danish
    "DE" - German
    "EL" - Greek
    "EN-GB" - English (British)
    "EN-US" - English (American)
    "EN" - English (unspecified variant for backward compatibility; please select EN-GB or EN-US instead)
    "ES" - Spanish
    "ET" - Estonian
    "FI" - Finnish
    "FR" - French
    "HU" - Hungarian
    "IT" - Italian
    "JA" - Japanese
    "LT" - Lithuanian
    "LV" - Latvian
    "NL" - Dutch
    "PL" - Polish
    "PT-PT" - Portuguese (all Portuguese varieties excluding Brazilian Portuguese)
    "PT-BR" - Portuguese (Brazilian)
    "PT" - Portuguese (unspecified variant for backward compatibility; please select PT-PT or PT-BR instead)
    "RO" - Romanian
    "RU" - Russian
    "SK" - Slovak
    "SL" - Slovenian
    "SV" - Swedish
    "ZH" - Chinese

text - string, array of strings or object containing strings.

key - your deepl api key.

### ussage ###

require __DIR__ . '/vendor/johncube/deepl/functions.php';

echo deepl('your-api-key', "test tłumaczenia deepl", "EN");