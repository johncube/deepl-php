<?php
    function singleDeepl($key, $text, $target_lang, $source_lang = null){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api-free.deepl.com/v2/translate');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "auth_key=$key&text=$text&target_lang=$target_lang&source_lang=$source_lang");
        
        $headers = array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            return false;
        }
        else if (curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200){
            return false;
        }
        $translatedWords = json_decode($result, true); // Decode the word
        $result = $translatedWords['translations'][0]['text']; // Search the word
        return $result;
        curl_close($ch);
    }

    function deepl($key, $text, $target_lang, $source_lang = null) {
        if(gettype($text) == "string"){
            return singleDeepl($key, $text, $target_lang, $source_lang);
        }
        $translations = array();
        foreach($text as $textKey => $textValue){
            $translations[$textKey] = singleDeepl($key, $textValue, $target_lang, $source_lang);
        }
        return $translations;
    }
?>